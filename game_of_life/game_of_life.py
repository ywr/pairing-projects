from random import randint
from copy import deepcopy
import time

class Grid(object):

    def __init__(self, size):
        self.size = size
        self.grid = []

    """initializes the grid with zeros"""
    def initialize_grid(self):
        for row in range(self.size):
            self.grid.append([])
        for col in range(self.size):
            for i in self.grid:
                i.append(0)

    def add_boarder(self):
        for row in range(self.size):
            self.grid[row].insert(0, '@')
            self.grid[row].insert(0, '@')
            self.grid[row].append('@')
            self.grid[row].append('@')
        horizon_boarder = []
        for i in range(self.size + 5):
            horizon_boarder.append('@')
        self.grid.insert(0, horizon_boarder)
        self.grid.insert(0, horizon_boarder)
        self.grid.append(horizon_boarder)
        self.grid.append(horizon_boarder)

    def visualize(self):
        for i in self.grid:
            list_of_cell = []
            for j in i:
                if isinstance(j, int):
                    list_of_cell.append(j)
            if len(list_of_cell) == self.size:
                print(list_of_cell)

    """A testing function for the whole game board"""
    def visualize_all(self):
        print(self.grid)

    def random_initialize(self):
        for i in range(self.size):
            for j in range(self.size):
                self.grid[i][j] = randint(0, 1)

    """making the cell live or die"""
    def make_live(self, x_coordinate, y_coordinate):
        self.grid[y_coordinate][x_coordinate] = 1

    def make_dead(self, x_coordinate, y_coordinate):
        self.grid[y_coordinate][x_coordinate] = 0

    """count the surrounding live cells"""
    def cell_count(self, x_coordinate, y_coordinate):
        life_count = 0
        for row in range(x_coordinate - 1, x_coordinate + 2):
            for col in range(y_coordinate - 1, y_coordinate + 2):
                if self.grid[row][col] == 1:
                    life_count += 1
        life_count -= 1 if self.grid[x_coordinate][y_coordinate] == 1 else 0
        return life_count

    """helper functions to see if a live or dead cell shall transform"""
    def live_will_die(self, x_coordinate, y_coordinate):
        if self.cell_count(x_coordinate, y_coordinate) != (2 or 3):
            return True

    def dead_will_come_alive(self, x_coordinate, y_coordinate):
        if self.cell_count(x_coordinate, y_coordinate) == 3:
            return True

    """the process in every single round"""
    def process(self):
        duplicate = deepcopy(self.grid)
        for i in range(len(self.grid)):
            for j in range(len(self.grid)):
                if self.grid[i][j] == 1:
                    if self.live_will_die(i, j):
                        duplicate[i][j] = 0
                if self.grid[i][j] == 0:
                    if self.dead_will_come_alive(i, j):
                        duplicate[i][j] = 1
        self.grid = deepcopy(duplicate)


game = Grid(10)


if __name__ == '__main__':
    game.initialize_grid()
    game.random_initialize()
    game.add_boarder()
    while True:
        game.process()
        game.visualize()
        print(10 * '-')
        time.sleep(3)
