from game_of_life import *

game_grid = Grid(5)
game_grid.initialize_grid()
game_grid.random_initialize()
game_grid.add_boarder()
game_grid.visualize()