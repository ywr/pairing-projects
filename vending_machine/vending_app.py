class Machine(object):

    def __init__(self):
        self.valid_coins = {'nickels': 1, 'dimes': 1, 'quarters': 1}
        self.current_amount = 0
        self.products = {'cola': [1, 7], 'crisps': [0.5, 10], 'chocolate': [0.65, 8]}

    def accept_coins(self, coins):
        if coins in self.valid_coins:
            self.current_amount += self.valid_coins[coins]
            print(self.current_amount)
        else:
            print('Please insert valid coins')

    def press_button(self, product):
        self.products[product][1] -= 1
        self.current_amount -= self.products[product][0]
        print('Enjoy your', product)
        print('You have %s dollars in this machine' % self.current_amount)


vending_machine = Machine()


if __name__ == '__main__':
    while True:
        input_coins = input('Please insert coins. Press "Done" to finish payment')
        if input_coins != 'Done':
            vending_machine.accept_coins(input_coins)
            print('You have %s dollars in this machine' % vending_machine.current_amount)
        else:
            break
    while True:
        input_product = input('What do you want? Press "Done" to finish your purchase')
        if input_product == 'Done':
            print('Please keep your change with %s dollars' % vending_machine.current_amount)
            break
        else:
            if vending_machine.current_amount >= vending_machine.products[input_product][0]:
                if vending_machine.products[input_product][1] == 0:
                    print('sold out')
                    print('You have %s dollars in this machine' % vending_machine.current_amount)
                else:
                    vending_machine.press_button(input_product)
            else:
                print('Insufficient amount inserted')
                print('You have %s dollars in this machine' % vending_machine.current_amount)
