import random


class Game(object):

    def __init__(self):
        self.frame = 10
        self.players = []

    def create_player(self, name):
        self.players.append(Player(name))

class Player(object):

    def __init__(self, name):
        self.name = name
        self.points = []

class Frame(object):

    def __init__(self):
        self.points = 0
        self.rolls = 2
        self.pins = 10

    def frame_end(self):
        if self.rolls == 0 or self.pins == 0:
            print('frame over')
            return True
        return False

    def roll(self):
        score = random.randint(0, self.pins)
        self.rolls -= 1
        self.pins -= score
        self.points += score
        print('You\'ve knocked down %s pins, your score so far is %s' % (score, self.points))
        return score

    def get_rolls(self):
        return self.rolls

    def get_pins(self):
        return self.pins

    def get_points(self):
        return self.points


g = Game()
g.create_player('Andrew')


def play():
    test_frame = Frame()
    rolls = []
    while not test_frame.frame_end():
        rolls.append(test_frame.roll())
        print('points:', test_frame.get_points(), ' pins left:', test_frame.get_pins(), ' rolls left:', test_frame.get_rolls())
    g.players[0].points.append(rolls)

def calculate_score(history_score):
    sumscore = 0
    index = 0
    for i in history_score:
        for j in i:
            if len(i) == 2:
                sumscore += j
            else:
                try:
                    if len(history_score[index + 1]) == 1:
                        sumscore += j + sum(history_score[index + 1]) + history_score[index + 2][0]
                    else:
                        sumscore += j + sum(history_score[index + 1])
                except IndexError:
                    sumscore += j
        index += 1
    return sumscore


if __name__ == '__main__':
    while g.frame >= 0:
        print('this is the %s th frame' % (11 - g.frame))
        print('your history score is %s' % g.players[0].points)
        print('your total score is %s' % calculate_score(g.players[0].points))
        g.frame -= 1
        play()
